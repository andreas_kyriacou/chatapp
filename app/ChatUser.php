<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;

  class ChatUser extends Model
  {
      protected $table = 'chat_users';
      protected $fillable = ['first_name','last_name', 'username', 'role'];
  }
