<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;

  class ChatMessage extends Model
  {
      protected $table = 'chat_messages';
      protected $fillable = ['msg','msg_type', 'sender_id', 'recipient_id', 'scheduled_for'];
  }
