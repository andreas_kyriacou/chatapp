<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use App\ChatMessage;
use App\ChatRoom;
use App\ChatUser;
use App\ChatRoomUser;
use Illuminate\Validation\Rule;

class MessagesController extends Controller
{
  private $sender = null;
  private $recipient = null;
  private $recipient_type = null;
  private $msg_type = null;
  private $msg = null;
  private $scheduled_for = null;

  public function setMessageSender($sender){
    $this->sender = $sender;
    return $this;
  }

  public function setMessageRecipient($recipient){
    $this->recipient = $recipient;
    return $this;
  }

  public function setMessage($msg){
    $this->msg = $msg;
    return $this;
  }

  public function setMessageType($msg_type){
    $this->msg_type = $msg_type;
    return $this;
  }

  public function setScheduleFor($scheduled_for){
    $this->scheduled_for = $scheduled_for;
    return $this;
  }

  public function send()
  {
    if(!isset($this->sender))
      return back()->with('error', 'Sender is not defined!');
    if(!isset($this->recipient))
      return back()->with('error', 'Recipient is not defined!');
    if(!isset($this->msg_type))
      return back()->with('error', 'Message Type is not defined');
    if(!isset($this->msg))
      return back()->with('error', 'Message is empty!');

    $new_message = new ChatMessage([
      'sender_id'=>$this->sender,
      'recipient_id'=>$this->recipient,
      'msg'=>$this->msg,
      'msg_type'=>$this->msg_type,
      'scheduled_for'=>$this->scheduled_for,
    ]);
    try {
      $new_message->save();
      return back()->with('success', 'Message sent');
    } catch (\Exception $e) {
      return back()->with('error', $e->getMessage());
    }
  }

  public function sendUserMessage(Request $request)
  {
    if ($request->isMethod('get')):
      return view('messages.form');
    else:
      $data = $request->all();
      $validation_rules=[
        'sender_id'=>'required|exists:chat_users,id',
        'recipient_id'=>'required|exists:chat_users,id',
        'msg'=>'required',
        'msg_type'=>'required',
        'scheduled_for'=>'sometimes|date_format:Y-m-d H:i:s',
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $sender_user = ChatUser::where('id', $data['sender_id'])->get(['role'])->first();
        $new_message = $this->setMessageSender($data['sender_id'])
                            ->setMessageRecipient($data['recipient_id'])
                            ->setMessageType('user')
                            ->setMessage($data['msg']);
        if(($sender_user->role=='Admin') && (isset($data['scheduled_for']))):
          $new_message = $this->setScheduleFor($data['scheduled_for']);
        endif;
        $new_message->send();
        return back();
      endif;

    endif;
  }

  public function sendRoomMessage(Request $request)
  {
    if ($request->isMethod('get')):
      return view('messages.roomform');
    else:
      $data = $request->all();
      $validation_rules=[
        'sender_id'=>'required|exists:chat_users,id',
        'recipient_id'=>'required|exists:chat_rooms,id',
        'msg'=>'required',
        'msg_type'=>'required',
        'scheduled_for'=>'sometimes|date_format:Y-m-d H:i:s',
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $check_user_joined = ChatRoomUser::where('room_id', $data['recipient_id'])->where('user_id', $data['sender_id'])->first();
        $sender_user = ChatUser::where('id', $data['sender_id'])->get(['role'])->first();
        if(!$check_user_joined && $sender_user->role=='Regular'):
          return back()->with('error', 'You have not joined this room, only admins can send messages');
        else:
          $new_message = $this->setMessageSender($data['sender_id'])
                              ->setMessageRecipient($data['recipient_id'])
                              ->setMessageType('room')
                              ->setMessage($data['msg']);
          if(($sender_user->role=='Admin') && (isset($data['scheduled_for']))):
            $new_message = $this->setScheduleFor($data['scheduled_for']);
          endif;
          $new_message->send();
          return back();
        endif;
      endif;
    endif;
  }

  public function sendMessageToAllRooms(Request $request)
  {
    if ($request->isMethod('get')):
      return view('messages.form');
    else:
      $data = $request->all();
      $validation_rules=[
        'sender_id'=>'required|exists:chat_users,id',
        'recipient_id'=>'required|exists:chat_rooms,id',
        'msg'=>'required',
        'msg_type'=>'required',
        'scheduled_for'=>'sometimes|date_format:Y-m-d H:i:s',
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $sender_user = ChatUser::where('id', $data['sender_id'])->get(['role'])->first();
        if($sender_user->role=='Admin'):
          $rooms = ChatRoom::all();
          foreach ($rooms as $r) {
            $new_message = $this->setMessageSender($data['sender_id'])
                                ->setMessageRecipient($r->id)
                                ->setMessageRecipientType('room')
                                ->setMessage($data['msg']);
            if(isset($data['scheduled_for'])):
              $new_message = $this->setScheduleFor($data['scheduled_for']);
            endif;
            $new_message->send();
            return back();
          }
        else:
          return back()->with('error', 'Not allowed to send to all rooms, the user has to be an admin');
        endif;
      endif;

    endif;
  }

}
