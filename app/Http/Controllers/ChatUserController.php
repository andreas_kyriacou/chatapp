<?php

namespace App\Http\Controllers;

use App;
use Validator;
use App\ChatRoom;
use App\ChatUser;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ChatUserController extends Controller
{
    public function list()
    {
      $users = ChatUser::all();
      return view('chatusers.list', compact('users'));
    }

    /*
      If request method is get, return the view with the form to create a new chatuser
      else check all data and save the user in db
    */
    public function create(Request $request)
    {
      if ($request->isMethod('get')):
        return view('chatusers.form');
      else:
        $data = $request->all();
        $validation_rules=[
          'first_name' => 'required',
          'last_name' => 'required',
          'username' => 'required|unique:chat_users',
          'role' => ['required', Rule::in(['Regular', 'Admin'])]
        ];
        $validation = Validator::make($data, $validation_rules);
        if($validation->fails()):
          return back()->with('error', $validation->errors());
        else:
          $chat_user = new ChatUser([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'role' => $data['role']
          ]);
          try{
            $chat_user->save();
            return back()->with('success', 'User was successfully created');
          } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
          }
        endif;
      endif;
    }

    //show details of the user, and rooms available to join
    public function show($user_id)
    {
      $data = ['user_id' => $user_id];
      $validation_rules=[
        'user_id'=>'required|exists:chat_users,id',
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $user = ChatUser::find($user_id);
        $rooms = ChatRoom::all();
        return view('chatusers.show', compact('user', 'rooms'));
      endif;
    }

}
