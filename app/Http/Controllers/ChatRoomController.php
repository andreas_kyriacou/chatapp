<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;
use App\ChatRoom;
use App\ChatRoomUser;

class ChatRoomController extends Controller
{

    public function list()
    {
      $rooms = ChatRoom::all();
      return view('chatrooms.list', compact('rooms'));
    }

    /*
      If request method is get, return the view with the form to create a new chatroom
      else check all data and save the chat room in db
    */
    public function create(Request $request)
    {
      if ($request->isMethod('get')):
        return view('chatrooms.form');
      else:
        $data = $request->all();
        $validation_rules=[
          'room_name' => 'required|unique:chat_rooms|max:255'
        ];
        $validation = Validator::make($data, $validation_rules);

        if($validation->fails()):
          return back()->with('error', $validation->errors());
        else:
          $chat_room = new ChatRoom([
            'room_name' => $data['room_name'],
            'no_of_users' => 0
          ]);
          try {
            $chat_room->save();
            return back()->with('success', 'Room was successfully created');
          } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
          }
        endif;
      endif;
    }

    //show details of the room
    public function show($room_id)
    {
      $data = ['room_id' => $room_id];
      $validation_rules=[
        'room_id'=>'required|exists:chat_rooms,id',
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $room = ChatRoom::find($room_id);
        return view('chatrooms.show', compact('room'));
      endif;
    }

    public function joinRoom($room_id, $user_id)
    {
      $data = [
        'room_id' => $room_id,
        'user_id' => $user_id
      ];
      $validation_rules=[
        'room_id'=>'required|exists:chat_rooms,id',
        'user_id'=>'required|exists:chat_users,id'
      ];
      $validation = Validator::make($data, $validation_rules);
      if($validation->fails()):
        return back()->with('error', $validation->errors());
      else:
        $chat_room_user_exists = ChatRoomUser::where('room_id', $room_id)->where('user_id', $user_id)->first();
        if($chat_room_user_exists==null):
          $chat_room_user = new ChatRoomUser([
            'room_id' => $room_id,
            'user_id' => $user_id
          ]);
          try {
            $chat_room_user->save();
            DB::table('chat_rooms')->whereId($room_id)->increment('no_of_users');
            return back()->with('success', 'User joined the room!');
          } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
          }

        else:
          return back()->with('error', 'User already joined this room');
        endif;
      endif;
    }
}
