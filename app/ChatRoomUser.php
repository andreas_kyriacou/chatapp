<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;

  class ChatRoomUser extends Model
  {
      protected $table = 'chat_room_users';
      protected $fillable = ['room_id','user_id'];
  }
