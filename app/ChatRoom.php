<?php
  namespace App;
  use Illuminate\Database\Eloquent\Model;

  class ChatRoom extends Model
  {
      protected $table = 'chat_rooms';
      protected $fillable = ['room_name','no_of_users'];
  }
