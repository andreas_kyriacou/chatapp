# ChatApp #

This is a very small and simple app to emulate:
* create chat room
* create chat user
* user joins a chat room
* user sends a message to another user or to chatroom


# Technologies used: #
* Laravel framework
* PHP 7.1.3
* MySQL for the db


# Notes: #
* I've created very simple views to include some of the forms, no css or anything else used.
* All the coding for the login is in the "app/Http/Controllers" folder and their models in the "app" folder
* All routes defined are in the "routes/web.php" file.
