<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('chatrooms/list', 'ChatRoomController@list');
Route::get('chatrooms/{room_id}/show', 'ChatRoomController@show');
Route::match(['get', 'post'], 'chatrooms/create', 'ChatRoomController@create');
Route::get('chatrooms/joinroom/{room_id}/{user_id}', 'ChatRoomController@joinRoom');

Route::get('chatusers/list', 'ChatUserController@list');
Route::match(['get', 'post'], 'chatusers/create', 'ChatUserController@create');
Route::get('chatusers/{user_id}/show', 'ChatUserController@show');

Route::match(['get', 'post'], 'send/user/message', 'MessagesController@sendUserMessage');
Route::match(['get', 'post'], 'send/room/message', 'MessagesController@sendRoomMessage');
