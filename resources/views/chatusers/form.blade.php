<form method="post" action="{{url('chatusers/create')}}">
{{csrf_field()}}

  @if ($message = Session::get('success'))
    <div>
       {{ Session::get('success') }}
     </div>
  @endif
  @if ($message = Session::get('error'))
    <div>
      {{ Session::get('error') }}
    </div>
  @endif

  <h4>New User</h4>

  <div>
    <div>
      <label for="first_name">First name: </label>
      <div>
        <input type="text" id="first_name" name="first_name">
      </div>
    </div>

    <div>
      <label for="last_name">Last name: </label>
      <div>
        <input type="text" id="last_name" name="last_name">
      </div>
    </div>

    <div>
      <label for="username">Username: </label>
      <div>
        <input type="text" id="username" name="username">
      </div>
    </div>

    <div>
      <label for="role">Role: </label>
      <div>
        <select class="" name="role">
          <option value="Admin">Admin</option>
          <option value="Regular">Regular</option>
        </select>
      </div>
    </div>
  </div>

<button type="submit">Submit</button>

</form>
