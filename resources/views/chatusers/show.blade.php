
@if ($message = Session::get('success'))
  <div>
     {{ Session::get('success') }}
   </div>
@endif
@if ($message = Session::get('error'))
  <div>
    {{ Session::get('error') }}
  </div>
@endif


<h4>Name: {{$user->first_name}} {{$user->last_name}}</h4>
<div>Username: {{$user->username}}</div>
<div>Role: {{$user->role}}</div>

<h4>Available Rooms:</h4>

<table>
  <tr>
    <th>Room Name</th>
  </tr>
  @foreach($rooms as $r)
    <tr>
      <td>{{$r->room_name}}</td>
      <td>
        <a href="/chatrooms/joinroom/{{$r->id}}/{{$user->id}}">Join</a> | <a href="">Send Room message</a>
      </td>
    </tr>
  @endforeach

</table>
