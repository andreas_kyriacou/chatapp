

  <h4>
    <div>
      <strong>Users</strong>
    </div>
    <div>
      <a href="/chatusers/create" class="btn btn-primary text-white">New</a>
    </div>
  </h4>


  <table>
    <thead>
      <tr>
        <th>User</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $u)
        <tr>
          <td>{{$u->first_name}} {{$u->last_name}} | {{$u->username}} | {{$u->role}} | <a href="/chatusers/{{$u->id}}/show">Show</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
