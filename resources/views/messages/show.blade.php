
@if ($message = Session::get('success'))
  <div>
     {{ Session::get('success') }}
   </div>
@endif
@if ($message = Session::get('error'))
  <div>
    {{ Session::get('error') }}
  </div>
@endif


<h4>Room name: {{$room->room_name}}</h4>
<div>Number of users in room: {{$room->no_of_users}}</div>
