
@if ($message = Session::get('success'))
  <div>
     {{ Session::get('success') }}
   </div>
@endif
@if ($message = Session::get('error'))
  <div>
    {{ Session::get('error') }}
  </div>
@endif

  <div>
    <h4>
      <div>
        <strong>Rooms</strong>
      </div>
      <div>
        <a href="/chatrooms/create" class="btn btn-primary text-white">New</a>
      </div>
    </h4>

    <table>
      <thead>
        <tr>
          <th>Room Name</th>
        </tr>
      </thead>
      <tbody>
        @foreach($rooms as $r)
          <tr>
            <td><a href="/chatrooms/{{$r->id}}/show">{{$r->room_name}}</a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
