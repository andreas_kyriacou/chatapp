<form method="post" action="{{url('send/user/message')}}">
{{csrf_field()}}
@if ($message = Session::get('success'))
  <div>
     {{ Session::get('success') }}
   </div>
@endif
@if ($message = Session::get('error'))
  <div>
    {{ Session::get('error') }}
  </div>
@endif

<h4>New Message</h4>

<div>
  <div>
    <input type="text" id="sender_id" name="sender_id" value="1">Sender Admin
  </div>
  <div>
    <input type="text" id="recipient_id" name="recipient_id" value="2">Recipient
  </div>
  <div>User or Chatroom
    <select name="msg_type">
      <option><option>
      <option value="user">User</option>
      <option value="room">Room</option>
    </select>
  </div>
  <div>
    <input type="text" id="scheduled_for" name="scheduled_for" value="2018-08-29 13:01:39">scheduled_for
  </div>
  <div>
    <textarea name="msg">this is the message</textarea>
  </div>
</div>

<button type="submit">Submit</button>

</form>
