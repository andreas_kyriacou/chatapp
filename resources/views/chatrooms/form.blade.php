<form method="post" action="{{url('chatrooms/create')}}">
{{csrf_field()}}

@if ($message = Session::get('success'))
  <div>
     {{ Session::get('success') }}
   </div>
@endif
@if ($message = Session::get('error'))
  <div>
    {{ Session::get('error') }}
  </div>
@endif


<h4>New Room</h4>

<div>
  <label for="room_name">Room Name: </label>
  <div>
    <input type="text" id="room_name" name="room_name">
  </div>
</div>

<button type="submit">Submit</button>

</form>
